package com.ofss.cityBankController;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ofss.resources.RestJavaNetClient;
import com.ofss.resources.RestJavaNetInfinityClient;




@WebServlet("/RestServiceController")
public class RestServiceController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action=request.getParameter("action");
		if(action.equals("kyc")) {

            RestJavaNetInfinityClient restobj=new RestJavaNetInfinityClient();
             List<String> list=restobj.fetchKYC();
            request.setAttribute("list", list);
            request.getRequestDispatcher("result.jsp").forward(request, response);
      
        }
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		doGet(request, response);
	}

}
