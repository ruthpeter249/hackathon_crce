var citiesByState = {
Odisha: ["Bhubaneswar","Puri","Cuttack"],
Maharashtra: ["Mumbai","Pune","Nagpur"],

AndhraPradesh: ["Adoni","Amaravati","Anantapur","Chandragiri", "Chittoor","Dowlaiswaram","Eluru","Guntur","Kadapa","Kakinada", "Kurnool" ,"Machilipatnam ","Nagarjunako??a ","Rajahmundry ","Srikakula" ,"Tirupati"," Vijayawada"," Visakhapatnam "," Vizianagaram ","Yemmiganur"],
WestBengal:["Alipore", "Alipur Duar","Asansol "," Baharampur","Bally ","Balurghat","Bankura","Baranagar","Barasat","Barrackpore","Bhatpara","Bishnupur","Budge Budge","Burdwan","Chandernagore","Darjiling","Diamond Harbour","Dum Dum","Durgapur","Halisahar","Haora","Hugli"," Ingraj"," Bazar", "Jalpaiguri", "Kalimpong", "Kamarhati", "Kanchrapara", "Kharagpur", "Koch", "Bihar", "Kolkata"," Krishnanagar"," Malda"," Midnapore"," Murshidabad", "Navadwip"," Palashi ","Panihati" ,"Purulia" ,"Raiganj" ,"Santipur" ,"Shantiniketan ","Shrirampur ","Siliguri" ,"Siuri ","Tamluk ","Titagar"],
Kerala: ["Alappuzha", "Badagara","Idukki","Kannur","Kochi","Kollam","Kottayam","Kozhikode","Mattancheri","Palakkad","Thalassery","Thiruvananthapuram","Thrissur"],
ArunachalPradesh: ["Itanagar"],
Bihar: [ "Ara","Baruni","Begusarai","Bettiah","Bhagalpur","  Bihar Sharif"," Bodh Gaya","Buxar","Chapra","Darbhanga","Dehri"," Dinapur Nizamat","Gaya","Hajipur","Jamalpur",  "Madhubani",
    "Katihar","Motihari","Munger","Muzaffarpur","Patna","Purnia","Pusa","Saharsa","Samastipur","Sasaram","Sitamarhi","Siwan"],
Chandigarh:["Chandigarh"],
Chhattisgarh:["Ambikapur","Bhilai","Bilaspur","Dhamtari","Durg","Jagdalpur","Raipur","Rajnandgaon"],
 //Dadra/NagarHaveli: ["Silvassa"]

}


function makeSubmenu(value) {
if(value.length==0) document.getElementById("citySelect").innerHTML = "<option></option>";
else {
var citiesOptions = "";
for(cityId in citiesByState[value]) {
citiesOptions+="<option>"+citiesByState[value][cityId]+"</option>";
}

document.getElementById("citySelect").innerHTML = citiesOptions;
}
}
function displaySelected() { var country = document.getElementById("countrySelect").value;
var city = document.getElementById("citySelect").value;
alert(country+"\n"+city);
}
function resetSelection() {
document.getElementById("countrySelect").selectedIndex = 0;
document.getElementById("citySelect").selectedIndex = 0;
}
