<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>City Bank Registration </title>

<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
<style>

.pt-32{
	padding-top : 20rem !important;
}
</style>
</head>
<body  onload="resetSelection()">
<div class="container">
<div class="row">

		<div class="col-lg-6">
	<form class="border border-light p-5" method="post" action="ValidationController">

 <input type="hidden" name="action" value="bank_acc_create"> 

    <p class="h4 mb-4 text-center">Bank Account Creation</p>
    
<div class="md-form">
  <i class="fas fa-user prefix"></i>
    <input type="text" id="defaultRegisterFormFirstName" class="form-control" name="fname" value="${fname }" placeholder="First name">   
</div>
 <div style="color:red;"> ${fnamemsg}</div>
<div class="md-form">
  <i class="fas fa-user prefix"></i>
    <input type="text" id="defaultRegisterFormMiddleName" class="form-control" placeholder="Middle name" name="mname" value="${mname }">   
${mnamemsg}
</div>

<div class="md-form">
  <i class="fas fa-user prefix"></i>
    <input type="text" id="defaultRegisterFormLastName" class="form-control" placeholder="Last name"  name="lname" value="${lname }">   
${lnamemsg}
</div>
	<div class="md-form">
	<i class="fas fa-home prefix"></i>
    <input type="text" id="defaultRegisterFormCurrentAddress" class="form-control mb-4" placeholder="Current Address" name="caddress" value="${caddress }">
	${caddressmsg}
	</div>
	<div class="md-form">
	<i class="fas fa-home prefix"></i>
    <input type="text" id="defaultRegisterFormPermanentAddress" class="form-control mb-4" placeholder="Permanent Address" name="paddress"value="${paddress }" >
	${paddressmsg}
	</div>
	
	<div class="md-form">
	
	<select id="countrySelect" size="1" onchange="makeSubmenu(this.value)" class="browser-default custom-select">
<option value="" disabled selected>Choose State</option>
<option>Andaman/Nicobar Islands</option>
<option>AndhraPradesh</option>
<option>Assam</option>
<option>Bihar</option>
<option>Chandigarh</option>
<option>Chhattisgarh</option>
<option>Dadra/NagarHaveli</option>
<option>Daman/Diu</option>
<option>Goa</option>
<option>Gujarat</option>
<option>Himachal Pradesh</option>
<option>Haryana</option>
<option>Jammu/Kashmir</option>
<option>Jharkhand</option>
<option>Karnataka</option>
<option>Kerala</option>
<option>Lakshadweep</option>
<option>Maharashtra</option>

<option>Mizoram</option>
<option>Manipur</option>

<option>Madhya Pradesh</option>
<option>Meghalaya</option>

<option>Nagaland</option>
<option>New Delhi</option>
<option>Odisha</option>

<option>Pondicherry</option>
<option>Punjab</option>

<option>Rajasthan</option>

<option>Sikkim</option>

<option>Tamil Nadu</option>
<option>Tripura</option>

<option>Uttaranchal</option>
<option>Uttar Pradesh</option>

<option>WestBengal</option>
</select>
	</div>
	
	<div class="md-form">
	
<select id="citySelect" size="1" class="browser-default custom-select">
<option value="" disabled selected>Choose City</option>
<option></option>

</select>
	
	</div>

	
	<div class="md-form">
	<i class="fas fa-phone prefix"></i>
   <input type="text" id="defaultRegisterPhonePassword" class="form-control"  name="phonenumber" value="${phonenumber }" placeholder="Phone number" aria-describedby="defaultRegisterFormPhoneHelpBlock">
   ${phonenumbermsg}
   </div>
<div class="md-form">
  <i class="fas fa-envelope prefix"></i>
    <input type="email" id="defaultRegisterFormEmail" class="form-control mb-4" placeholder="E-mail" name="email" value="${email }" >
${emailmsg}
</div>

<div class="md-form">
  <i class="fas fa-lock prefix"></i>
    <input type="password" id="defaultRegisterFormPassword" name="password"  class="form-control" placeholder="Password" aria-describedby="defaultRegisterFormPasswordHelpBlock">
${passwordmsg}
</div>
    <small id="defaultRegisterFormPhoneHelpBlock" class="form-text text-muted mb-4">Minimal 8 characters lenght</small>


<div class="md-form">
  <i class="fas fa-lock prefix"></i>
    <input type="password" id="passwdInput" class="form-control mb-4" placeholder="Re Enter Your Password" name="repassword">
${repasswordmsg}
</div>
   
    <button class="btn btn-info my-4 btn-block" type="submit">Register</button>

    <div class="text-center">
        
	
        <hr>
        
    </div>
</form>
   <hr>

	</div>

		<div class="col-lg-6 pt-32">
		<div>
		  <img src="images/hdcclogo.PNG" >
		</div>
		
		<div> 
		    <img src="images/Infinitybank.JPG" height="50%" width="50%" style="margin-left:50%;">
		</div>    
		<br/>
			<a href="test.jsp">Click</a>
		<form action="RestServiceController" method="post" class="pt-4">
        <input type="hidden" name="action" value="kyc_verify_option"> 
      
       	Have a verfied KYC account in another bank? 
       	Verify your account using Other bank!!!
       	  <button class="btn btn-info my-4 btn-block" type="submit">Click Here for Instant Verification!</button>
</form>


	</div>
	
</div>
</div>
</body>


<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/js/mdb.min.js"></script>
<script type="text/javascript" src="js/city.js"></script>

</html>