package com.ofss.resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("HdccclientMessage")
public class RestJavaNetClient {

	 List<String> list=new ArrayList  <String>();
	@GET
	@Produces(MediaType.APPLICATION_JSON)
    public  List<String> fetchKYC(String aadharno){
		String url1 ="http://10.180.40.134:8022/Hdcc_Bank_Api/webapi/Hdcc_Customers";
		StringBuilder str=new StringBuilder(url1);
		str.append("/");
		str.append(aadharno);
	
		System.out.println(str.toString());
		String url =str.toString();
        HttpURLConnection urlConn = null;
        BufferedReader reader = null;
        try {
            URL urlObj = new URL(url);
            urlConn = (HttpURLConnection) urlObj.openConnection();
            urlConn.setRequestMethod("GET");
            urlConn.setConnectTimeout(5000);
            urlConn.setReadTimeout(5000);
            urlConn.setRequestProperty("Accept", "application/json");
            if (urlConn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                System.err.println("Unable to connect to the URL...");
                return null;
            }
            System.out.println("Connected to the server...");
            InputStream is = urlConn.getInputStream();
            reader = new BufferedReader(new InputStreamReader((is)));
            System.out.println("Reading data from server...");
            
            
            String tmpStr = null;
            while((tmpStr = reader.readLine()) != null){
                System.out.println(tmpStr);
                list.add(tmpStr);
            }
           
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            try {
                if(reader != null) reader.close();
                if(urlConn != null) urlConn.disconnect();
            } catch(Exception ex){
                 
            }
        }
        return list;}
}

