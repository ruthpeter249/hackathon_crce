package com.ofss.cityBankController;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ofss.resources.RestJavaNetClient;
import com.ofss.resources.RestJavaNetInfinityClient;




@WebServlet("/RestServiceController")
public class RestServiceController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action=request.getParameter("action");
		if(action.equals("kyc")) {
			String aadharno=request.getParameter("aadharno");
			String bankOption=request.getParameter("bankOption");
			System.out.println(bankOption);
			if(bankOption.equals("HDCC Bank")) {
				 RestJavaNetClient restobj=new RestJavaNetClient();
	             List<String> list=restobj.fetchKYC(aadharno);
	            request.setAttribute("list", list);
	            request.getRequestDispatcher("result.jsp").forward(request, response);
			}else {
            RestJavaNetInfinityClient restobj=new RestJavaNetInfinityClient();
             List<String> list=restobj.fetchKYC();
            request.setAttribute("list", list);
            request.getRequestDispatcher("result.jsp").forward(request, response);
			}
        }
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action=request.getParameter("action");
		//doGet(request, response);
		if(action.equals("kyc_verify_option")) {
			request.getRequestDispatcher("kycoption.jsp").forward(request, response);
		}
		
	}

}
