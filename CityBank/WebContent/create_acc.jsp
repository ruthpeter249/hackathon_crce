<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>City Bank Registration </title>

<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
</head>
<body>
<div class="row">
	<div class="col-lg-4">
	
	</div>
		<div class="col-lg-4">
	<form class="border border-light p-5" method="post" action="ValidationController">

 <input type="hidden" name="action" value="bank_acc_create"> 

    <p class="h4 mb-4 text-center">Bank Account Creation</p>
<div class="md-form">
  <i class="fas fa-user prefix"></i>
    <input type="text" id="defaultRegisterFormFirstName" class="form-control" name="fname" value="${fname }" placeholder="First name">   
</div>
 <div style="color:red;"> ${fnamemsg}</div>
<div class="md-form">
  <i class="fas fa-user prefix"></i>
    <input type="text" id="defaultRegisterFormMiddleName" class="form-control" placeholder="Middle name" name="mname" value="${mname }">   
${mnamemsg}
</div>

<div class="md-form">
  <i class="fas fa-user prefix"></i>
    <input type="text" id="defaultRegisterFormLastName" class="form-control" placeholder="Last name"  name="lname" value="${lname }">   
${lnamemsg}
</div>
	<div class="md-form">
	<i class="fas fa-home prefix"></i>
    <input type="text" id="defaultRegisterFormCurrentAddress" class="form-control mb-4" placeholder="Current Address" name="caddress" value="${caddress }">
	${caddressmsg}
	</div>
	<div class="md-form">
	<i class="fas fa-home prefix"></i>
    <input type="text" id="defaultRegisterFormPermanentAddress" class="form-control mb-4" placeholder="Permanent Address" name="paddress"value="${paddress }" >
	${paddressmsg}
	</div>
	<div class="md-form">
	<i class="fas fa-phone prefix"></i>
   <input type="text" id="defaultRegisterPhonePassword" class="form-control"  name="phonenumber" value="${phonenumber }" placeholder="Phone number" aria-describedby="defaultRegisterFormPhoneHelpBlock">
   ${phonenumbermsg}
   </div>
<div class="md-form">
  <i class="fas fa-envelope prefix"></i>
    <input type="email" id="defaultRegisterFormEmail" class="form-control mb-4" placeholder="E-mail" name="email" value="${email }" >
${emailmsg}
</div>

<div class="md-form">
  <i class="fas fa-lock prefix"></i>
    <input type="password" id="defaultRegisterFormPassword" name="password"  class="form-control" placeholder="Password" aria-describedby="defaultRegisterFormPasswordHelpBlock">
${passwordmsg}
</div>
    <small id="defaultRegisterFormPhoneHelpBlock" class="form-text text-muted mb-4">Minimal 8 characters lenght</small>


<div class="md-form">
  <i class="fas fa-lock prefix"></i>
    <input type="password" id="passwdInput" class="form-control mb-4" placeholder="Re Enter Your Password" name="repassword">
${repasswordmsg}
</div>
   
    <button class="btn btn-info my-4 btn-block" type="submit">Register</button>

    <div class="text-center">
        

        <hr>

    </div>
</form>
	
	</div>
		<div class="col-lg-4">
	
	</div>
</div>
</body>


<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/js/mdb.min.js"></script>
</html>