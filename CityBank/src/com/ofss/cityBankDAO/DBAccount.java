package com.ofss.cityBankDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import com.ofss.cityBankBeans.CityBankCustomer;

public class DBAccount {
	Connection con;
	PreparedStatement pstmt;
	ArrayList<CityBankCustomer> list;
	public void setConnection(Connection con) {
	this.con = con;
	}
	public ArrayList<CityBankCustomer> getUsers() throws SQLException {
			list = new ArrayList<CityBankCustomer>();
			 String sql = "select CITY_CUST_FIRSTNAME, CITY_CUST_EMAILID from CITY_CUSTOMER";
			PreparedStatement pstmt = con.prepareStatement(sql);
			//pstmt.setString(1, cat_id);
			ResultSet rst =pstmt.executeQuery();
			while(rst.next()){
			String namedb=rst.getString("CITY_CUST_FIRSTNAME");
			String emaildb=rst.getString("CITY_CUST_EMAILID");
			System.out.println("name:"+namedb);
			System.out.println("email:"+emaildb);
			CityBankCustomer c = new CityBankCustomer();
			c.setFname(namedb);
			c.setEmail(emaildb);
			list.add(c);
			c=null;
			}
			return list;
		}
	public void  insertCustomer(String fname, String lname, String email, String password) throws SQLException {
		 String sql = "insert into CITY_CUSTOMER(CITY_CUSTID,CITY_CUST_FIRSTNAME,CITY_CUST_LASTNAME,CITY_CUST_EMAILID,CITY_CUST_PASSWORD)values ('CT'||CUSTOMER_CUSTID.NEXTVAL,?,?,?,?)";
			PreparedStatement pstmt = con.prepareStatement(sql);
			pstmt.setString(1, fname);
			pstmt.setString(2, lname);
			pstmt.setString(3, email);
			pstmt.setString(4, password);
			pstmt.executeUpdate();//executeUpdate for insert
		System.out.println("just inserted db");
	}
	
	
	public boolean checkLogin(String email, String password) throws ClassNotFoundException, SQLException {
		System.out.println("inside login code ");
		String sql="select * from CITY_CUSTOMER";//put the first statement of sql query as string
		pstmt=con.prepareStatement(sql);//convert into sql query
		ResultSet result=pstmt.executeQuery();
		list=new ArrayList<CityBankCustomer>();
		while(result.next()){
			String uemail=result.getString("CITY_CUST_EMAILID");
			String upassword=result.getString("CITY_CUST_PASSWORD");
			CityBankCustomer c=new CityBankCustomer();
		c.setEmail(uemail);
		c.setPassword(upassword);
			list.add(c);
		}
		Iterator itr=list.iterator();
	while(itr.hasNext()){
		CityBankCustomer c=(CityBankCustomer) itr.next();
		if(c.getEmail().equals(email) && c.getPassword().equals(password)){
			return true;
		}
	
	}return false;
	}
	
	}

